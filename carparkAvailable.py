# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 2021

@author: agustin.rombola
"""
import requests

import pandas as pd
import datetime 

#Headers 
h = {'AccountKey' : '',
     'accept' : 'application/json'} #Use your own account key
url = 'http://datamall2.mytransport.sg/ltaodataservice/CarParkAvailabilityv2'
carpark = []

#Getting data
while True:
  new = requests.get(url,headers=h,params={'$skip': len(carpark)}).json()['value']
  if new == []:
    break
  else:
    carpark += new
#Getting date and hour    
now = datetime.datetime.now()

#Getting csv file
finalData = pd.DataFrame.from_dict(carpark)
finalData=finalData.assign(Date=now)
finalData.to_csv('carparkavailable.csv')

#Uncomment for xlsx file
#finalData.to_excel("carparkavailable.xlsx")  

