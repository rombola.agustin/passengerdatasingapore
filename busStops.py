# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 2021

@author: agustin.rombola
"""

import requests
import pandas as pd
import datetime 

#Headers 
h = {'AccountKey' : '',
     'accept' : 'application/json'} #Use your own account key
url = 'http://datamall2.mytransport.sg/ltaodataservice/BusStops'
busstops = []

#Getting data
while True:
  new = requests.get(url,headers=h,params={'$skip': len(busstops)}).json()['value']
  if new == []:
    break
  else:
    busstops += new
finalData = pd.DataFrame.from_dict(busstops)

#Getting date and hour    
now = datetime.datetime.now()
finalData=finalData.assign(Date=now)

#Getting csv file
finalData.to_csv('busStops.csv')

#Uncomment to get xlsx file
#finalData.to_excel("busstops.xlsx")  

