"""
Created on Mon Aug  2 2021

@author: agustin.rombola
"""

import requests
import json
import pandas as pd
from datetime import datetime


#Use your AccountKey here!
h = {'AccountKey' : '',
     'accept' : 'application/json'} #Use your own account key

def getCSVfile(url):
    response = requests.get(url,headers=h)
    
    if response.status_code == 200:
        print ('Request Successfully')
            
        data = response.content
        jsonObj = json.loads(data)
        #Get the URL
        urlData = jsonObj["value"][0]["Link"]
        #print('URL is', urlData)
        
        #Download zip file
        myfile = requests.get(urlData)
        open('data.zip', 'wb').write(myfile.content)
        
        from zipfile import ZipFile
        test_file_name = "data.zip"
        #Extract all
        with ZipFile(test_file_name, 'r') as zip:
            fileName = zip.namelist()[0]
            zip.extractall() 
        print(f'{fileName} extrated successfully')

    else:
        print ('Error in request')
        print(response.content)
    return fileName

# Created a column with date and hour
def makeDate(fileName):
    df = pd.read_csv(fileName)
    df['DATE_GENERATED'] = pd.to_datetime(df['YEAR_MONTH'],format='%Y-%m', errors='ignore')
    df['DATE_GENERATED'] = df['DATE_GENERATED'] + pd.to_timedelta(df['TIME_PER_HOUR'],unit='h')
    
    df['DATE_GENERATED'] = df['DATE_GENERATED'].dt.strftime('%m/%Y  %H:%M')
    #print(type(df['DATE_GENERATED']))
    
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    df = df.assign(DATE_CREATED = now)
    df.to_csv (fileName)
    print('Date added successfully')
    return

# URLS

urls = ['http://datamall2.mytransport.sg/ltaodataservice/PV/Train',
        'http://datamall2.mytransport.sg/ltaodataservice/PV/ODTrain',
        'http://datamall2.mytransport.sg/ltaodataservice/PV/ODBus',
        'http://datamall2.mytransport.sg/ltaodataservice/PV/Bus',
        ]


# ------------------------------------------------------------------
# Main program
for i in urls:
    fileName = getCSVfile(i)
    makeDate(fileName)
    
#Delete data.zip
from os import remove
try:
    remove("data.zip")
except:
    pass