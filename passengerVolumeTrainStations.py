# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 2021

@author: agustin.rombola
"""

import requests
import json
p = {'Date':'202106'} #Format yyyy/mm/dd 
p = {}
h = {'AccountKey' : '',
     'accept' : 'application/json'} #Use your own account key

def getCSVfile(url):
    response = requests.get(url,headers=h)
    
    if response.status_code == 200:
        print ('Request Successfully!')
            
        data = response.content
        jsonObj = json.loads(data)
        urlData = jsonObj["value"][0]["Link"]
        #print('URL is', urlData)
        
        myfile = requests.get(urlData)
        open('data.zip', 'wb').write(myfile.content)
        
        from zipfile import ZipFile
        
        test_file_name = "data.zip"
        
        with ZipFile(test_file_name, 'r') as zip:
            fileName = zip.namelist()[0]
            zip.extractall() 
        print(fileName)
    else:
        print ('Error in request')
        print(response.content)
urls = ['http://datamall2.mytransport.sg/ltaodataservice/PV/Train',
        'http://datamall2.mytransport.sg/ltaodataservice/PV/ODTrain',
        'http://datamall2.mytransport.sg/ltaodataservice/PV/ODBus',
        'http://datamall2.mytransport.sg/ltaodataservice/PV/Bus',
        ]
for i in urls:
    getCSVfile(i)